if [ ! -f /content/vietTTS/assets/infore/hifigan/g_01140000 ]; then
  echo "Downloading models..."
  mkdir -p /content/vietTTS/assets/infore/{nat,hifigan}
  wget https://huggingface.co/ntt123/viettts_infore_16k/resolve/main/duration_latest_ckpt.pickle -O /content/vietTTS/assets/infore/nat/duration_latest_ckpt.pickle
  wget https://huggingface.co/ntt123/viettts_infore_16k/resolve/main/acoustic_latest_ckpt.pickle -O /content/vietTTS/assets/infore/nat/acoustic_latest_ckpt.pickle
  wget https://huggingface.co/ntt123/viettts_infore_16k/resolve/main/g_01140000 -O /content/vietTTS/assets/infore/hifigan/g_01140000
  python3 -m vietTTS.hifigan.convert_torch_model_to_haiku --config-file=/content/vietTTS/assets/hifigan/config.json --checkpoint-file=/content/vietTTS/assets/infore/hifigan/g_01140000
fi

echo "Generate audio clip"
text=`cat /content/vietTTS/assets/transcript.txt`
python3 -m vietTTS.synthesizer --text "$text" --output /content/vietTTS/assets/infore/clip.wav --lexicon-file /content/vietTTS/assets/infore/lexicon.txt --silence-duration 0.2
